import React from 'react';
import { AppContext } from './../contexts/AppContext';
import Errors from './../components/Errors';


// Router
import { Link } from "react-router-dom";

class Register extends React.Component {

  static contextType = AppContext;

  render() {
    
    const { handleInput, register, state } = this.context;

    return (
      <div className="uk-container uk-flex uk-flex-middle uk-flex-center">
        <div className="register-page">
        <h1>Register</h1>
          <form className="uk-form" action="" method="post" onSubmit={ register } >
            <div className="uk-margin">
              <div className="uk-inline">
                <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: mail"></span>
                <input className="uk-input uk-form-width-large uk-form-blank" onChange={ handleInput } value={ state.email } name="email" type="email" placeholder="E-mail" />
              </div>
            </div>
            <div className="uk-margin">
              <div className="uk-inline">
                  <input className="uk-input uk-form-width-large uk-form-blank" onChange={ handleInput } value={ state.password } name="password" type="password" placeholder="Password" />
                  <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
              </div>
            </div>
            <div className=".uk-margin">
              <button className="uk-button uk-button-primary">{ state.registerText }</button>
            </div>
          </form>
        </div>
      </div>

    );

  }

}

export default Register;