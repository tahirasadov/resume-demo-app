import React from 'react';
import fire from './../config/Firebase/firebase';

// Components
import ProfileView from './../components/ProfileView';


class View extends React.Component {
  
  state = {}

  async componentDidMount(){

    let username = this.props.match.params.username.toLowerCase();
    
    this.setState( { username, id: null, loading: true } );

    let userRef = fire.firestore().collection( 'userDetails' );

    await userRef.where( "username", "==", username )
    .limit( 1 )
    .get()
    .then( ( snapshot ) => {
      this.setState( { loading: false } );
      snapshot.forEach( ( doc ) => {
        this.setState( { id: doc.id, ...doc.data() });
      } );

    } );
    
  }

  render() {

    return (
      
      <div className="uk-container uk-flex uk-flex-middle uk-flex-center">
        <div className="front-page">

          { (!this.state.loading && this.state.id == null ) && (
            <h1>Not resume found!</h1>
          ) }

          { this.state.loading &&
            <div className="uk-position-center">
            <div uk-spinner="ratio: 1"></div>
          </div> }

          { this.state.id && (
            <ProfileView info={ this.state } />
          ) }

        </div>
      </div>

    );

  }

}

export default View;