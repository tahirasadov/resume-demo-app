import React from 'react';

// Router
import { Link } from "react-router-dom";

class Home extends React.Component {

  render() {

    return (

      <div className="uk-container uk-flex uk-flex-middle uk-flex-center">
        <div className="front-page">
          <Link to="/login" className="uk-button">Add your resume</Link>
        </div>
      </div>

    );

  }

}

export default Home;