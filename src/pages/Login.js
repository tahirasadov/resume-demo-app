import React from 'react';
import { AppContext } from './../contexts/AppContext';
import fire from './../config/Firebase/firebase';
import Errors from './../components/Errors';

// Router
import { Link } from "react-router-dom";

class Login extends React.Component {

  static contextType = AppContext;

  componentDidMount() {
    fire.auth().onAuthStateChanged( ( user ) => {
      
      if( user ) {
        const { logout } = this.context;
      }
      
    } );
  
}

  render() {
    
    const { handleInput, login, state } = this.context;

    return (

      <div className="uk-container uk-flex uk-flex-middle uk-flex-center">
        <div className="login-page">
          <h1>Login</h1>
          <form action="" method="post" onSubmit={ login } >
          { Object.keys(state.errors).length > 0 && <Errors errors={ state.errors } /> }
            <div className="uk-margin">
              <div className="uk-inline">
                <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: mail"></span>
                <input required className="uk-input uk-form-width-large uk-form-blank" onChange={ handleInput } value={ state.email } name="email" type="email" placeholder="E-mail" />
              </div>
            </div>
            <div className="uk-margin">
              <div className="uk-inline">
                <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                <input required className="uk-input uk-form-width-large uk-form-blank" onChange={ handleInput } value={ state.password } name="password" type="password" placeholder="Password" />
              </div>
            </div>
            <div className=".uk-margin">
              <input type="submit" value={ state.loginBtnText } className="uk-button uk-button-primary" />{" "}
              <Link className="uk-button uk-button-secondary" to="/register">Register</Link>
            </div>
          </form>
        </div>
      </div>

    );

  }

}

export default Login;