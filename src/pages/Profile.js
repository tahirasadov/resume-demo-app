import React from 'react';
import { AppContext } from '../contexts/AppContext';
import fire from './../config/Firebase/firebase';

//Router
import { withRouter } from "react-router-dom";

import ProfileForm from './../components/ProfileForm';
import { showWarningMessage } from '../helpers/helpers';

class Profile extends React.Component {
  
 
  componentDidMount() {
      fire.auth().onAuthStateChanged( ( user ) => {
        
        if( !user ) {
          showWarningMessage( 'Please login first!' );
          this.props.history.push( '/login' );
        }
        
      } );
    
  }
  


  static contextType = AppContext;

  render() {

    const { handleInput, update, state } = this.context;

    return (<React.Fragment>

      { !state.user &&
        <div className="uk-position-center">
        <div uk-spinner="ratio: 1"></div>
      </div> }

      { state.user && <ProfileForm /> }

    </React.Fragment>)

  }

}

export default withRouter( Profile );