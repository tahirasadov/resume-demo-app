import React from 'react';
import fire, { storage } from './../config/Firebase/firebase';
import { showError, showErrorMessage, showWarningMessage, showSuccessMessage, closeAllNotifications } from './../helpers/helpers';

//Router
import { withRouter } from "react-router-dom";

// Validation
import registerFormRules from '../config/register_form_rules';
import profileFormRules from '../config/profile_form_rules';
import validate, { username_exists }         from './../helpers/validation';

export const AppContext = React.createContext();

class AppContextProvider extends React.Component {

  constructor( props ) {
    super( props );
    
    this.handleInput = this.handleInput.bind( this );
    this.login       = this.login.bind( this );
    this.register    = this.register.bind( this );
    this.update      = this.update.bind( this );

    
    this.state = {
      firstname: '',
      lastname : '',
      jobtitle : '',
      username : '',
      password : '',
      website  : '',
      behance  : '',
      facebook : '',
      dribbble : '',
      email    : '',
      phone    : '',
      errors   : {},
      registerText: 'Submit',
      loginBtnText: 'Login',
      updateBtnText: 'Update',
    }
  }

  avatar = {}

  handleInput( e ) {

    this.setState( {
      [ e.target.name ]: e.target.value
    } );
    
    if( e.target.name == 'avatar' ){
      this.avatar = e.target.files[ 0 ];
    }
    
  }

  componentDidMount() {
    this.autListener();

  }



  autListener() {

    fire.auth().onAuthStateChanged( ( user ) => {
      if( user ) {
        this.setState( { user} );
        
        let userId = fire.auth().currentUser.uid;
        
        
        let userRef = fire.firestore().collection( 'userDetails' );
        userRef.where( "userId", "==", userId )
        .limit( 1 )
        .get()
        .then( ( snapshot ) => {

          snapshot.forEach( ( doc ) => {

            this.setState( { user, id: doc.id, ...doc.data() } );            
            
          } );
        } );

        
      }else {
        this.setState( { user: false } );
      }
    } );

  }

  async update( e ) {

    e.preventDefault();

    this.setState( {

      updateBtnText: 'Updating...',
      errors: {}

    } );

    let errors = validate( profileFormRules, this.state );


    for( let field in errors ) {

      for( let id in errors[ field ]) {

        let error = errors[ field ][ id ];

        showErrorMessage( error );

      }
      
    }
    if( Object.keys(errors).length === 0 ) {

      let userId      = fire.auth().currentUser.uid;
      
      if( await username_exists( this.state.username ) ) {

        showErrorMessage( 'This username is used by another account' );

      }else {

        let profileData = {
          userId:     userId,
          username:   this.state.username,
          firstname:  this.state.firstname,
          lastname:   this.state.lastname,
          jobtitle:   this.state.jobtitle,
          website:   this.state.website,
          behance:   this.state.behance,
          facebook:   this.state.facebook,
          dribbble:   this.state.dribbble,
          phone:   this.state.phone,
          email:   this.state.email,
        }
        
        fire.firestore().collection( 'userDetails' )
        .doc( userId )
        .get()
        .then( doc => {
          if( doc.exists ) {
            fire.firestore().collection( 'userDetails' ).doc( userId ).update( profileData );
          }else {
            fire.firestore().collection( 'userDetails' ).doc( userId ).set( profileData );
          }
        } )

        if( this.avatar.type ){
          
          if( this.avatar.type == 'image/jpeg' ){
            this.setState( {

              updateBtnText: 'Uploading image...'
        
            } );

            await storage.ref().child( 'avatars' )
            .child( userId )
            .child( this.avatar.name )
            .put( this.avatar )
            .then( response => response.ref.getDownloadURL() )
            .then( photoURL => fire.firestore().collection( 'userDetails' ).doc( userId ).update( { avatar: photoURL } ) );
            
          } else {
            
            showErrorMessage( 'Image mime type must be jpeg' );
            
          }

        }

      }
      
    }

    this.setState( {

      updateBtnText: 'Update',
      errors: {}

    } );
    window.open( '/@/' + this.state.username, "_blank")

  }

  logout() {
    fire.auth().signOut();
    this.props.history.push( '/profile/' );
  }

  async login( e ) {
    
    e.preventDefault();

    let errors = {};

    this.setState( {

      loginBtnText: 'Loggin in...',
      errors: {}

    } );


    await fire.auth().signInWithEmailAndPassword( this.state.email, this.state.password )
    .then( ( user ) => {
      
      if( user.user.emailVerified ){
        this.props.history.push( '/profile/' );
        
      }else {

        showWarningMessage( 'Please verify your email first' );

        user.user.sendEmailVerification().then(function() {

          showSuccessMessage( 'Verification email is sent' );

        }).catch(function(error) {
          
          showErrorMessage( error.message );

        });
        this.props.history.push( '/login/' );
      }
      

    } )
    .catch( ( error ) => {
      
      showErrorMessage( error.message );

    } );


    this.setState( {

      loginBtnText: 'Login'

    } );
    
  }

  async register( e ) {

    e.preventDefault();

    closeAllNotifications();
    
    this.setState( {

      registerText: 'Submitting...'

    } );

    let errors = validate( registerFormRules, this.state );

    for( let field in errors ) {

      for( let id in errors[ field ]) {

        let error = errors[ field ][ id ];

        showErrorMessage( error );

      }
      
    }

    if( Object.keys(errors).length === 0 ) {

      let does_username_exists = await username_exists( this.state.username );

      if( !does_username_exists && !does_username_exists) {
        
        await fire.auth().createUserWithEmailAndPassword( this.state.email, this.state.password ).then( ( user ) => {
          
          user.user.sendEmailVerification().then(function() {

            showSuccessMessage( 'Verification email is sent' );

          }).catch(function(error) {
            
            showErrorMessage( error.message );

          });
          
          
        } ).catch( ( error ) => {

          showErrorMessage( error.message );

        } );
        
      }
      
    }

    this.setState( {

      registerText: 'Submit'
      
    } );

    this.props.history.push( '/login/' );


  }


  render() {

    return(
      <AppContext.Provider value={ { state: this.state, handleInput: this.handleInput, login: this.login, register: this.register, update: this.update } }>
        { this.props.children }
      </AppContext.Provider>
    );

  }

}


export default withRouter( AppContextProvider );