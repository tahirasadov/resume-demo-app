import React from 'react';

const Errors = ( props ) => {

  let errorFields = props.errors;

  let errors = [];

  for( let field in errorFields ){
    for( let error in errorFields[ field ] ) {
      errors.push( errorFields[ field ][ error ] );
    } 
  }

  let ErrorList = errors.map( ( error, key ) => {
    
    return <p key={ key } >{ error }</p>
    
  } );

  return <>
    <div className="uk-alert-danger" uk-alert={ "" }>
      { ErrorList }
    </div>
  </>

}

export default Errors;