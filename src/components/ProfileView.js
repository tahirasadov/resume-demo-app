import React from 'react';

export default ( props ) => {
  
  return ( 
    <div className="uk-card uk-card-default uk-card-body _uk-width-xxlarge uk-animation-slide-bottom-medium">
      <img src={ props.info.avatar } className="profile" />
      <h2 className="fullname" uk-tooltip="Full name">{ props.info.firstname + " " + props.info.lastname }</h2>
      <h4 uk-tooltip="Job title">{ props.info.jobtitle }</h4>
      <p className="uk-article-meta uk-text-left uk-text-emphasis">{ props.info.info }</p>

      <hr className="uk-divider-icon"></hr>
      <h2>Social</h2>

      <div className="portfolio-section">
        
        { props.info.website && (
          <a href={ props.info.website } target="_blank">
            <span uk-icon="icon: world; ratio: 1"></span> Website
          </a>
        ) }
        { props.info.dribbble && (
          <a className="dribble" href={ props.info.dribbble } target="_blank">
            <span uk-icon="icon: dribbble; ratio: 1"></span> Dribble
          </a>
        ) }
        { props.info.facebook && (
          <a className="facebook" href={ props.info.facebook } target="_blank">
            <span uk-icon="icon: behance; ratio: 1"></span> Behance
          </a>
        ) }
        { props.info.behance && (
          <a className="behance" href={ props.info.facebook } target="_blank">
            <span uk-icon="icon: facebook; ratio: 1"></span> Facebook
          </a>
        ) }

      </div>
      
      <hr className="uk-divider-icon"></hr>
      <h2>Contact</h2>

      <div className="portfolio-section">
      { props.info.email && (
        
        <a href={`mailto:${props.info.email}`} target="_blank">
          <span uk-icon="icon: mail; ratio: 1"></span> { props.info.email }
        </a>
      ) }

       { props.info.phone && (
        <span>
          <span uk-icon="icon: phone; ratio: 1"></span> { props.info.phone }
        </span>
      ) }
        
      </div>

  </div>
  );

}