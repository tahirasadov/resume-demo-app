import React from 'react';
import { AppContext } from '../contexts/AppContext';

export default class ProfileForm extends React.Component {
  
  static contextType = AppContext;

  render() {

    const { handleInput, update, state } = this.context;

    return(

      <div className="uk-container uk-container-xsmall">
        <div className="profile-page">
          <h2>Update your profile</h2>
          <br />
          <form method="post" onSubmit={ update }>

          <fieldset className="uk-fieldset">

            <legend className="uk-legend">General</legend>

            <div className="uk-grid-small" uk-grid={ " " }>
              <div className="uk-width-1-2">
                <div className="uk-margin">
                  <div className="uk-inline">
                    <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                    <input className="uk-input uk-form-blank"  onChange={ handleInput } value={ state.firstname } name="firstname" type="text" placeholder="First name" />
                  </div>
                </div>
              </div>

              <div className="uk-width-1-2">
                <div className="uk-margin">
                  <div className="uk-inline">
                    <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                    <input className="uk-input uk-form-blank"  onChange={ handleInput } value={ state.lastname } name="lastname" type="text" placeholder="Last name" />
                  </div>
                </div>
              </div>
              <div className="uk-width-1-1">
                <div className="uk-margin">
                  <div className="uk-inline">
                    <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                    <input className="uk-input uk-form-blank"  onChange={ handleInput } value={ state.username } name="username" type="text" placeholder="Username" />
                  </div>
                </div>
              </div>

              <div className="uk-width-1-1">
                <div className="uk-margin">
                  <div className="uk-inline">
                    <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: user"></span>
                    <input className="uk-input uk-form-blank"  onChange={ handleInput } value={ state.jobtitle } name="jobtitle" type="text" placeholder="Job title" />
                  </div>
                </div>
              </div>
          </div>





          </fieldset>

          <fieldset className="uk-fieldset">
              <legend className="uk-legend">Portfolio</legend>

              <div uk-grid={ " " }>
                <div className="uk-width-1-2">
                  <div className="uk-margin">
                    <div className="uk-inline">
                      <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: world"></span>
                      <input className="uk-input uk-form-blank" onChange={ handleInput } value={ state.website } name="website" type="text" placeholder="Web site" />
                    </div>
                  </div>
                </div>
                <div className="uk-width-1-2">
                  <div className="uk-margin">
                    <div className="uk-inline">
                      <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: behance"></span>
                      <input className="uk-input uk-form-blank" onChange={ handleInput } value={ state.behance } name="behance" type="text" placeholder="Behance" />
                    </div>
                  </div>
                </div>
                <div className="uk-width-1-2">
                  <div className="uk-margin">
                    <div className="uk-inline">
                      <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: facebook"></span>
                      <input className="uk-input uk-form-blank" onChange={ handleInput } value={ state.facebook } name="facebook" type="text" placeholder="Facebook" />
                    </div>
                  </div>
                </div>
                <div className="uk-width-1-2">
                  <div className="uk-margin">
                    <div className="uk-inline">
                      <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: dribbble"></span>
                      <input className="uk-input uk-form-blank"  onChange={ handleInput } value={ state.dribbble } name="dribbble" type="text" placeholder="Dribbble" />
                    </div>
                  </div>
                </div>
              </div>

          </fieldset>


          <fieldset className="uk-fieldset">
                <legend className="uk-legend">Contact</legend>

                <div uk-grid={ " " }>
                  <div className="uk-width-1-2">
                    <div className="uk-margin">
                      <div className="uk-inline">
                        <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: mail"></span>
                        <input className="uk-input uk-form-blank"  onChange={ handleInput } value={ state.email } name="email" type="email" placeholder="E-mail" />
                      </div>
                    </div>
                  </div>

                  <div className="uk-width-1-2">
                    <div className="uk-margin">
                      <div className="uk-inline">
                        <span className="uk-form-icon uk-form-icon-flip" uk-icon="icon: phone"></span>
                        <input className="uk-input uk-form-blank" onChange={ handleInput } value={ state.phone } name="phone" type="text" placeholder="Phone" />
                      </div>
                    </div>
                  </div>
                </div>


              </fieldset>
          <fieldset className="uk-fieldset">
            <legend className="uk-legend">Profile image</legend>

            <div uk-grid={ " " }>
              <div className="uk-width-1-1">
                  <input className="uk-input uk-form-blank" onChange={ handleInput } name="avatar" type="file" />
              </div>
            </div>

          </fieldset>
          
          
          <div className="uk-margin">
            <button className="uk-button uk-button-primary">{ state.updateBtnText }</button>
          </div>

          </form>
        </div>
      </div>

    );

  }

} 