
import fire from './../config/Firebase/firebase';

export default ( fields = [], state ) => {

  const errors = {};

  for( let field in fields ) {
    
    let rules = fields[ field ];
    let value = state[ field ];
     
    for( let rule in rules ) {

      let message = rules[ rule ][ 'message' ];
      let passed  = eval( 'rule_' + rule + '( value , rules[ rule ])' );

      if( !passed ) {

        if( errors[ field ] === undefined ){
          errors[ field ] = [];
        }

        errors[ field ].push( message );

      }
    }
    
  }

  return errors;

}

function rule_not_empty( str ) {

  return str !== '';
  
}
function rule_minmax( str, params ) {

  if( str.length >= params.min && str.length <= params.max ) {
    return true;
  }

  return false;

}

function rule_email( email ) {

  let pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  
  return pattern.test(String(email).toLowerCase());
  
}


export async function username_exists( username ) {
  
  if( username == '' ){
    return false;
  }
  
  let userId = fire.auth().currentUser.uid;

  let username_exists = false;
  
  let userRef = fire.firestore().collection( 'userDetails' );
  
  await userRef.where( "username", "==", username ).get().then( ( snapshot ) => {
    
    snapshot.forEach( ( doc ) => {
      
      if( doc.data().userId != userId ){
        
        username_exists = true;
      
      }

    } );

  } );
    
  return username_exists;
  
}

