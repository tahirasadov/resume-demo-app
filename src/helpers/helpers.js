import UIkit from 'uikit';

function notify( message, status, position = 'top-left', timeout = 5000 ){
  UIkit.notification({
    message: message,
    status: status,
    pos: position,
    timeout: timeout
  });
}

export const showErrorMessage = ( message ) => {
  notify( message, 'danger' );
}

export const showWarningMessage = ( message ) => {
  notify( message, 'warning' );
}

export const showSuccessMessage = ( message ) => {
  notify( message, 'success' );
}

export const closeAllNotifications = () => {
  UIkit.notification.closeAll();
}
