export default {
  password: {
    not_empty: {
      message: "<b>Password</b> is required"
    },
    minmax: {
      message: "<b>Password</b> length must be between 6 and 40.",
      min: 6,
      max: 40
    }
  },
  email: {
    not_empty: {
      message: "<b>E-mail</b> is required"
    },
    email: {
      message: "Must be valid <b>e-mail</b>"
    },
    minmax: {
      message: "<b>E-mail</b> length must be between 4 and 100.",
      min: 4,
      max: 100
    }
  },
}
