export default {
  firstname: {
    minmax: {
      message: "First name length must be between 0 and 40.",
      min: 0,
      max: 40
    }
  },
  lastname: {
    minmax: {
      message: "Last name length must be between 0 and 40.",
      min: 0,
      max: 40
    }
  },
  username: {
    not_empty: {
      message: "Username is required"
    },
    minmax: {
      message: "Username length must be between 2 and 40.",
      min: 2,
      max: 40
    }
  }
}
