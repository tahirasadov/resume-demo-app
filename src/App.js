import React from 'react';
import './App.css';

// Context
import AppContextProvider from './contexts/AppContext';

// Router
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

// Pages
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import View from './pages/View';
import Profile from './pages/Profile';

// Components
// import Header  from './components/Header';
// import Sidebar from './components/Sidebar';

function App() {
  return (
    <Router>
      <AppContextProvider>
        <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/register">
          <Register />
        </Route>
        <Route exact path="/profile">
          <Profile />
        </Route>
        <Route exact path="/@/:username" component={ View } />
        </Switch>
      </AppContextProvider>
    </Router>
  );
}

export default App;
